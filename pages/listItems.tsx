import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import CloudUpload from '@material-ui/icons/CloudUpload';
import Input from '@material-ui/icons/Input';
import Send from '@material-ui/icons/Send';
import Link from '@material-ui/core/Link';

export const mainListItems = (
  <div>
    <Link href="/Dashboard">
        <ListItem button>
        <ListItemIcon>
            <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
        </ListItem>
    </Link>
    <Link href="/Incoming">
        <ListItem button>
        <ListItemIcon>
            <Input />
        </ListItemIcon>
        <ListItemText primary="Incoming" />
        </ListItem>
    </Link>
    <Link href="/Outgoing">
        <ListItem button>
        <ListItemIcon>
            <Send />
        </ListItemIcon>
        <ListItemText primary="Outgoing" />
        </ListItem>
    </Link>
  </div>
);

export const secondaryListItems = (
  <div>
    <ListSubheader inset>Actions</ListSubheader>
    <ListItem button>
      <ListItemIcon>
        <CloudUpload />
      </ListItemIcon>
      <ListItemText primary="Upload KPC" />
    </ListItem>
  </div>
);