import React from 'react';
import {
  PieChart, Pie, Tooltip, ResponsiveContainer, Cell
} from 'recharts';
import Title from './Title';


const data01 = [
  { name: 'Botswana', value: 11 }, { name: 'UAE', value: 15 },
  { name: 'Canada', value: 18 }, { name: 'Russia', value: 21 }
];

let renderLabel = function(entry: { name: any; }) {
  return entry.name;
}

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

export default function PieChart2(){

    return (
      <React.Fragment>
      <Title>Outgoing By Country</Title>
      <ResponsiveContainer>
        <PieChart width={400} height={400}>
        <Pie dataKey="value" isAnimationActive={true} data={data01} cx={150} cy={120} outerRadius={80} fill="#8884d8" label={renderLabel}>
            {
              data01.map((_entry,index) => <Cell fill={COLORS[index % COLORS.length]} />)
            }
          </Pie>
          <Tooltip />
        </PieChart>
      </ResponsiveContainer>
      </React.Fragment>
    );

}
