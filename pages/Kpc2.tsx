import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import CssBaseline from '@material-ui/core/CssBaseline';
import clsx from 'clsx';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import { mainListItems, secondaryListItems } from './listItems';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: '#757575',
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 120,
  },
  fixedHeightPie: {
    height: 300,
  },
  table: {
    minWidth: 650,
  },
  confirmed: {
    color: "green",
  },
}));


function createData(kpc_id: string, date: string, exporter: string, status: string) {
  return { kpc_id, date, exporter, status };
}

const rows = [
  createData('BW018434', '22/06/20', 'Okavango diamond', 'Confirm',),
  createData('BW018319', '20/06/20', 'Okavango diamond', 'Confirmed',),
  createData('AE101275', '19/06/20', 'DIACORE DMCC', 'Confirmed',),
  createData('EU00902096', '19/06/20', 'TACHE-HSK', 'Confirmed',),
  createData('EU00901574', '19/06/20', 'TACHE-HSK', 'Confirmed',),
];

export default function Kpc2() {
  const classes = useStyles();

  const [open, setOpen] = React.useState(true);
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };


  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            IL00050419
          </Typography>
          <IconButton color="inherit">
            <Badge badgeContent={4} color="secondary">
              <NotificationsIcon />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <List>{mainListItems}</List>
        <Divider />
        <List>{secondaryListItems}</List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Paper className={classes.paper}>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                Status:
              </Grid>
              <Grid item xs={10}>
                Awaiting Confirmation
              </Grid>
            </Grid>
            <br/>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                KPC ID:
              </Grid>
              <Grid item xs={10}>
                IL00050419
              </Grid>
            </Grid>
            <br/>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                Issue Date:
              </Grid>
              <Grid item xs={10}>
                26/01/2020
              </Grid>
            </Grid>
            <br/>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                Exporter:
              </Grid>
              <Grid item xs={10}>
                ROSY BLUE SALES LTD.
              </Grid>
            </Grid>
            <br/>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                Importer:
              </Grid>
              <Grid item xs={10}>
                HARI KRISHNA EXPORTS PVT LTD
                <br/>
                UNIT NO.1701 THE CAPITAL 17
                <br/>
                FL, B WING PLOT NO C70 BKC
                <br/>
                BANDRA (E)MUMBAI 400051 INDIA
              </Grid>
            </Grid>
            <br/>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                Total Carats:
              </Grid>
              <Grid item xs={10}>
                1648.26
              </Grid>
            </Grid>
            <br/>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                Value(US$):
              </Grid>
              <Grid item xs={10}>
                1,588,688.86
              </Grid>
            </Grid>
            <br/>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                Number of Parcels:
              </Grid>
              <Grid item xs={10}>
                1
              </Grid>
            </Grid>
            <br/>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                Country of Origin:
              </Grid>
              <Grid item xs={10}>
                SOUTH AFRICA
              </Grid>
            </Grid>
            <br/>
            <Grid container spacing={3}>
              <Grid item xs={2}>
                Expire Date:
              </Grid>
              <Grid item xs={10}>
                26/03/2020
              </Grid>
            </Grid>


          </Paper>
          <Paper>
            <img src={'/images/KP_IL00050419_2601.jpg'} width="100%"></img>
          </Paper>
        </Container>
      </main>
    </div>
  );
}